FROM node:slim
EXPOSE 8125
WORKDIR /app
COPY . /app
CMD [ "node", "index.js" ]
